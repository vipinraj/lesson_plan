namespace :lesson_plan do
  desc "Install Lesson Plan"
  task :install do
    system "rsync -ruv --exclude=.svn vendor/plugins/lesson_plan/public ."
    #system "cp vendor/plugins/lesson_plan/core/online_exam_controller.rb vendor/plugins/fedena_online_exam/app/controllers/  -Rf"
    system "cp vendor/plugins/lesson_plan/core/course.rb app/models/  -Rf"
    system "cp vendor/plugins/lesson_plan/core/batch.rb app/models  -Rf"
    system "git checkout config/locales/en.yml"
    #system "cp config/locales/en.yml config/locales/en_default.yml  -Rf"
    system "cp vendor/plugins/lesson_plan/core/locales/* config/locales/  -Rf"
  end
end
