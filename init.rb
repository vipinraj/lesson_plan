require 'translator'
require File.join(File.dirname(__FILE__), "lib", "lesson_plan")
#require 'acts_as_taggable'
require 'dispatcher'

FedenaPlugin.register = {
  :name=>"lesson_plan",
  :description=>"Lesson Plan Module",
  :auth_file=>"config/lesson_plan_auth.rb",
  #:dashboard_menu=>{:title=>"library_text",:controller=>"library",:action=>"index",\
      #:options=>{:class => "option_buttons", :id => "library_button", :title => "manage_library"}},
  #:student_profile_more_menu=>{:title=>"library_text",\
      #:destination=>{:controller=>"library",:action=>"student_library_details"}},
  #:employee_profile_more_menu=>{:title=>"library_text",\
      #:destination=>{:controller=>"library",:action=>"employee_library_details"}},
  #:css_overrides=>[{:controller=>"user",:action=>"dashboard"}],
  #:autosuggest_menuitems=>[
  #  #{:menu_type => 'link' ,:label => "autosuggest_menu.library",:value =>{:controller => :library,:action => :index}}
  #]
  
  :multischool_models=>%w{Topic Resource}
}

Dir[File.join("#{File.dirname(__FILE__)}/config/locales/*.yml")].each do |locale|
  I18n.load_path.unshift(locale)
end



LessonPlan.attach_overrides
