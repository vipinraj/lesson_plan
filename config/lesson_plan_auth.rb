authorization do

  role :lesson_planner do
    has_permission_on [:topics],
                      :to => [
                          :home,
                          :index,
                          :new,
                          :show,
                          :assign_topics,
                          :list_topics,
                          :add_topics,
                          :remove_topics,
                          :dash_board,
                          :edit,
                          :delete]

  end

  role :learner do
    has_permission_on [:topics],
                      :to => [
                          :home,
                          :assign_topics,
                          :list_topics,
                          :dash_board,
                          ]

  end



  # admin privileges
  role :admin do
    includes :lesson_planner
    end

  role :student do
    includes :lesson_planner
    end

  role :employee do
    includes :online_exam_control
    includes :lesson_planner

  end


end