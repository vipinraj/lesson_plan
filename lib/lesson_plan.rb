require 'dispatcher'
require 'patches/online_student_exam_controller'
require 'patches/online_exam_attendance_ext'
require 'patches/online_exam_group_ext'
require 'patches/user_controller'
require 'patches/user'
module LessonPlan


  def self.attach_overrides

    Dispatcher.to_prepare :lesson_plan do
      ::OnlineStudentExamController.instance_eval {include OnlineStudentExamController}
      ::OnlineExamAttendance.instance_eval {include OnlineExamAttendanceExt}
      ::OnlineExamGroup.instance_eval {include OnlineExamGroupExt}
      ::UserController.instance_eval {include UserController}
      ::User.instance_eval {include UserExt}
      Subject.send :has_and_belongs_to_many, :topics, :join_table => "subject_topics"
      User.send :has_many, :topics
      User.send :has_many, :resources
      User.send :has_many, :privilege_group_users
      User.send :has_many, :privilege_groups,:through=>:privilege_group_users

    end
  end


end

