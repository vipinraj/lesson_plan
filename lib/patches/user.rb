module LessonPlan
  module UserExt


    def self.included (base)
      base.instance_eval do
      	has_many :privilege_group_users
        has_many :privilege_groups,:through=>:privilege_group_users
        alias_method :role_symbols,:role_symbols_new
        has_many :program_group_users
        has_many :programme_groups, :through=> :program_group_users
      end
    end



    def valid_program_group_ids
      if self.admin or self.student or self.parent
        return ProgrammeGroup.active.collect(&:id)
      elsif self.employee
        if (self.role_symbols & ProgrammeGroup.management_roles).blank?
          return self.programme_group_ids
        else
          return ProgrammeGroup.active.collect(&:id)
        end
      end
    end

    def role_symbols_new
    prv = []
    privileges.map { |privilege| prv << privilege.name.underscore.to_sym } unless @privilge_symbols

    PrivilegeGroup.find_all_by_name(self.privileges.map{|x|x.name.underscore}).collect(&:privilege_name).compact.flatten.reject(&:blank?).map{|x| prv << x.underscore.to_sym}



    @privilge_symbols ||= if admin?
      [:admin] + prv
    elsif student?
      [:student] + prv
    elsif employee?
      [:employee] + prv
    elsif parent?
      [:parent] + prv
    else
      prv  
    end


  end



end
end