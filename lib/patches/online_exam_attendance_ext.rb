module LessonPlan
  module OnlineExamAttendanceExt


    def self.included (base)
      base.instance_eval do
        before_create :generate_session
      end
    end



    def generate_session
      self.session_id = SecureRandom.hex(5)
    end





  end

end