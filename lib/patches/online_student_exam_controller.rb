module LessonPlan
  module OnlineStudentExamController


    def self.included (base)
      base.instance_eval do
       alias_method_chain :start_exam, :spinov
      end
    end



    def start_exam_with_spinov
      @student = Student.find_by_user_id(current_user.id,:select=>"id,batch_id")
      @exam = @student.available_online_exams.find_by_id(params[:id].to_i)
      if @exam.present?
        unless @exam.already_attended(@student.id)
          @exam_attendance = OnlineExamAttendance.create(:online_exam_group_id=> @exam.id, :student_id=>@student.id, :start_time=>Time.now, :ip_address => request.remote_ip, :current_page => 1)
          if @exam.randomize_questions==true
            @exam_questions=@exam.online_exam_groups_questions.shuffle_it(current_user.id).paginate(:per_page=>5,:page=>params[:page], :include=>:online_exam_question)
          else
            @exam_questions=@exam.online_exam_groups_questions.paginate(:per_page=>5,:page=>params[:page],:order=>"position ASC", :include=>:online_exam_question)
          end
          @descriptive_answers = @exam_questions.select{|q| q.online_exam_question.question_format == "descriptive"}.map{|d| @exam_attendance.online_exam_score_details.build(:online_exam_question_id=>d.online_exam_question_id)}.group_by(&:online_exam_question_id)
          question_ids=@exam_questions.collect(&:id)
          @options=OnlineExamOption.all(:conditions=>{:id=>@exam_questions.collect(&:answer_ids).flatten.compact}).map {|op| @exam_attendance.online_exam_score_details.build(:online_exam_question_id=>op.online_exam_question_id, :online_exam_option_id=>op.id)}.group_by(&:online_exam_question_id)
        else
          render :partial => 'already_attended' and return
        end
        render :layout => false
      else
        flash[:notice]=t('flash_msg4')
        redirect_to :controller => 'user', :action => 'dashboard'
      end
    end





  end

end