module LessonPlan
  module OnlineExamGroupExt


    def self.included (base)
      base.instance_eval do
        has_many :online_exam_group_topics

        has_many :topics , :through => :online_exam_group_topics
      end
    end








  end

end