module LessonPlan
  module UserController



     def self.included (base)
      base.instance_eval do
       #alias_method :edit_privilege,:edit_privilege_new
       alias_method_chain :profile,:profile_new
      end
    end


    def edit_privilege_new
    @user = User.active.first(:conditions => ["username LIKE BINARY(?)",params[:id]])
    if @user.admin? or @user.student? or @user.parent? or current_user == @user
      flash[:notice] = "#{t('flash_msg4')}"
      redirect_to :controller => "user",:action => "dashboard"
    else
      @finance = Configuration.find_by_config_value("Finance")
      @sms_setting = SmsSetting.application_sms_status
      @hr = Configuration.find_by_config_value("HR")
      @privilege_tags=PrivilegeTag.find(:all,:order=>"priority ASC")
      @user_privileges=@user.privileges
      @privilege_groups=PrivilegeGroup.all
      if request.post?
        if params[:privilege_group_ids].present?
          privilege_groups = PrivilegeGroup.find(params[:privilege_group_ids].split(','))
          privilege_group_users = @user.privilege_group_users
            privilege_group_users.each do |pgu|
              pgu.destroy
              end
            privilege_groups.each do |pg|
            @user.privilege_group_users.create(:privilege_group_id=>pg.id)
            end
        end
        if params[:user].present? && params[:user][:programme_group_ids].present?
          @user.programme_group_ids = params[:user][:programme_group_ids]
        end
        new_privileges = params[:user][:privilege_ids] if params[:user]
        new_privileges ||= []

        @user.privileges = Privilege.find_all_by_id(new_privileges)
        @user.clear_menu_cache
        @user.delete_user_menu_caches
        flash[:notice] = "#{t('flash15')}"
        redirect_to :action => 'profile',:id => @user.username and return
      end
    end
    render :template=>"users/edit_privilege_new" and return
    end

    def profile_with_profile_new
      profile_without_profile_new
      render :template =>"users/profile_new"
    end

  end
end