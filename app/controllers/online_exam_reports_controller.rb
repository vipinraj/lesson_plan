class OnlineExamReportsController < ApplicationController
  before_filter :login_required
  before_filter :default_time_zone_present_time


  def exam_overview
    get_allowed_batches
  end


  def update_reset_exam
    @attendance = OnlineExamAttendance.find(params[:id])
    @online_exam_group = @attendance.online_exam_group
    ActiveRecord::Base.transaction do
      OnlineExamAttendance.hard_delete(params[:id])
    end
    flash[:notice]="#{t('exam_reset_successful_for_selected_students')}"
    redirect_to :action => :real_time_status, :id => @online_exam_group.id
  end


  def show_active_exam
    @batch = Batch.find_by_id(params[:batch_id])
    @exams=[]
    if @batch.present?
      @exams = @batch.online_exam_groups.paginate(:page => params[:page], :per_page => 20, :conditions => ["is_deleted = FALSE AND user_id = #{@current_user.id}"], :include => :online_exam_attendances, :order => "id DESC")
      #@exams = OnlineExamGroup.paginate(:page => params[:page], :per_page => 20 ,:conditions=>[ "batch_id = '#{params[:batch_id]}'"], :include=> :online_exam_attendances,:order=>"id DESC")
    end
    render :partial => 'active_exam_list', :locals => {:batch_id => params[:batch_id]}
  end


  def real_time_status
    @online_exam_group = OnlineExamGroup.find(params[:id])
    @attendances = @online_exam_group.online_exam_attendances
  end


  def view_answersheet
    @exam_attendance = OnlineExamAttendance.find(params[:id])
    @exam = @exam_attendance.online_exam_group
    @max_marks = @exam.online_exam_groups_questions.sum(:mark).to_f
    @exam_questions = @exam.online_exam_groups_questions.paginate(:per_page=>5,:page=>params[:page],:order=>"position ASC",:include=>[:online_exam_question])
    @student = Student.find(@exam_attendance.student_id)
    @answers = @exam_attendance.online_exam_score_details.all(:conditions=>{:online_exam_question_id=>@exam_questions.collect(&:online_exam_question_id)}).group_by(&:online_exam_question_id)
  end

  private


  def get_allowed_batches
    privilege = current_user.privileges.map { |p| p.name }
    if current_user.admin or privilege.include?("ExaminationControl") or privilege.include?("EnterResults")
      @batches = Batch.active
    elsif current_user.employee
      @batches= current_user.employee_record.subjects.all(:group => 'batch_id').map { |x| x.batch }.uniq.sort_by { |c| c.full_name }
    end
  end


end
