class TopicsController < ApplicationController
  before_filter :login_required


  def home
    redirect_to :action=> :dash_board if @current_user.student?
  end

  def index
    @topics = @current_user.topics
  end

  def new
    @topic = Topic.new
    if request.post?
      @topic = Topic.new(params[:topic])
      @topic.user_id = @current_user.id
      if @topic.save
        flash.now[:notice] = "Topic created successfully"
      redirect_to :action => "show", :id => @topic.id
        end
    end
  end

  def show
    @topic = Topic.find(params[:id])
    @resources = @topic.resources
  end

  def assign_topics
    @subjects = @current_user.employee_entry.subjects
    @topics = @current_user.topics

  end

  def list_topics
    @subject = Subject.find(params[:subject_id])
    @assigned_topics = @subject.topics
    @all_topics = @current_user.topics.reject { |topic| @subject.topics.include? topic }

    render :update do |page|
      if @subject.present?
        page.replace_html 'list_topics', :partial => 'assign_section'
      else
        page.replace_html 'list_topics', :text => ''
      end
    end
  end

  def add_topics
    @assigned_topics = Topic.find_all_by_id(params[:add_topic_ids].split(','))
    @subject = Subject.find(params[:subject_id])
    @assigned_topics.each do |t|
      @subject.topic_ids = @subject.topic_ids << t.id
    end
    @all_topics = @current_user.topics.reject { |topic| @subject.topics.include? topic }
    @assigned_topics = @subject.topics


    render :update do |page|
      page.replace_html 'list_topics', :partial => 'assign_section'
    end
  end

  def remove_topics
    @topics = Topic.find_all_by_id(params[:remove_topic_ids].split(','))
    @subject = Subject.find(params[:subject_id])
    @topics.each do |t|
      @subject.topics.delete(Topic.find (t))
    end
    @assigned_topics = @subject.topics
    @all_topics = @current_user.topics.reject { |topic| @subject.topics.include? topic }

    render :update do |page|
      page.replace_html 'list_topics', :partial => 'assign_section'
    end


  end

  def dash_board
    if @current_user.employee_entry.present?
    @subjects = @current_user.employee_entry.subjects
    elsif @current_user.student?
      @student = @current_user.student_record
      @batch = @student.batch
     general_subjects = Subject.find_all_by_batch_id(@batch.id, :conditions=>"elective_group_id IS NULL AND is_deleted=false")
     student_electives = StudentsSubject.find_all_by_student_id(@student.id,:conditions=>"batch_id = #{@batch.id}")
     elective_subjects = []
     student_electives.each do |elect|
      elective_subjects.push Subject.find(elect.subject_id)
     end
     @subjects = general_subjects + elective_subjects

    end
    if request.post? and params[:topic].present?
      @topic = Topic.find(params[:topic])
      @resources = @topic.resources if @topic.present?
      render(:update) do |page|
        page.replace_html 'report', :partial => "description"
      end
    elsif request.post?
      @subject = Subject.find(params[:subject_id])
      @topics = @subject.topics
      @topic = @topics.first
      @resources = @topic.resources if @topic.present?
      render(:update) do |page|
        page.replace_html 'student_list', :partial => "topic_list", :object => @topics
        @topic.nil? ? (page.replace_html 'report', :text => "") : (page.replace_html 'report', :partial => "description")
      end
    end

  end

  def edit
    @topic = Topic.find(params[:id])
    if request.post?
      @topic.update_attributes(params[:topic])
      redirect_to :action => "show", :id => @topic.id
    end
  end

  def delete
    @topic = Topic.find(params[:id])
    @topic.destroy
    redirect_to :action => "index"
  end


  def my_resources
   @resource = @current_user.resources.new
    @resources = @current_user.resources
  end



  def upload_resources

  @resource = @current_user.resources.new(params[:resource])

  @resource.save
  @resources = @current_user.resources
  redirect_to :action => :my_resources

  end


  def download_resources
    @resource = Resource.find(params[:id])
    send_file  @resource.attachment.path, :type=>@resource.attachment.content_type
  end

  def delete_resources
    @resource = Resource.find(params[:id])
    @resource.destroy
    redirect_to :action=> "my_resources"
  end


  def assign_resources
    @topic = Topic.find(params[:id])
    @assigned_resources = @topic.resources
    @available_resources = @current_user.resources.select{|x|!@assigned_resources.collect(&:id).include?(x.id)}
  end


  def add_resources
    @assigned_resources = Resource.find_all_by_id(params[:add_resources_ids].split(','))
    @topic = Topic.find(params[:topic])
    @assigned_resources.each do |t|
      @topic.resource_ids = @topic.resource_ids << t.id
    end
    @assigned_resources = @topic.resources
    @available_resources = @current_user.resources.select{|x|!@assigned_resources.collect(&:id).include?(x.id)}
    render :update do |page|
      page.replace_html 'list_resources', :partial => 'assign_resources'
    end
  end

  def remove_resources
    @resources = Resource.find_all_by_id(params[:remove_resource_ids].split(','))
    @topic = Topic.find(params[:topic_id])
    @resources.each do |t|
      @topic.resources.delete(Resource.find (t))
    end
    @assigned_resources = @topic.resources
    @available_resources = @current_user.resources.select{|x|!@assigned_resources.collect(&:id).include?(x.id)}

    render :update do |page|
      page.replace_html 'list_resources', :partial => 'assign_resources'
    end

  end


  def my_quiz

    if @current_user.employee_entry.present?
      @topics = @current_user.topics
      @online_exam_groups = @topics.collect(&:online_exam_groups).flatten.compact
    elsif @current_user.student
      redirect_to :controller=> :online_student_exam
    end
  end


end