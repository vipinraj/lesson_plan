class LessonPlansController < ApplicationController
  before_filter :login_required

  def subject_summary
    @subjects = @current_user.employee_record.subjects
  end


  def subject_students
    @subject = Subject.find(params[:id])
    @batch=Batch.find(@subject.batch_id)
    unless @subject.elective_group_id.nil?
      elective_student_ids = StudentsSubject.find_all_by_subject_id(@subject.id).map { |x| x.student_id }
      @students = Student.find_all_by_batch_id(@batch, :conditions=>"FIND_IN_SET(id,\"#{elective_student_ids.split.join(',')}\")")
    else
      @students = Student.find_all_by_batch_id(@batch.id)
    end
  end


end
