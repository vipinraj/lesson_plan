class Resource < ActiveRecord::Base
  has_and_belongs_to_many :topics, :join_table => "topic_resources"


  belongs_to :user

  validates_presence_of :name, :attachment

  has_attached_file :attachment,
                    :path => "uploads/:class/:attachment/:id_partition/:basename.:extension",
                    :url => "uploads/:class/:attachment/:id/:basename.:extension"

   validates_presence_of :attachment_file_name


  validates_attachment_size :attachment, :less_than => 5120000,\
    :message=>:must_be_less_than_5_mb


end
