class Topic < ActiveRecord::Base

  has_and_belongs_to_many :subjects,:join_table => "subject_topics"
  has_and_belongs_to_many :resources,:join_table => "topic_resources"
  has_many :online_exam_group_topics
  has_many :online_exam_groups, :through=> :online_exam_group_topics
  after_save :update_redactor
  attr_accessor :redactor_to_update, :redactor_to_delete
  before_destroy :delete_redactors

  validates_presence_of :name, :description

  xss_terminate :except => [:description]


  def update_redactor
    RedactorUpload.update_redactors(self.redactor_to_update,self.redactor_to_delete)
  end

  def delete_redactors
    RedactorUpload.delete_after_create(self.description)
  end

end
