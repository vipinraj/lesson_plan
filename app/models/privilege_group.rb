class PrivilegeGroup < ActiveRecord::Base
	serialize :privilege_name
	serialize :allowed_privilege
	serialize :allowed_privilege_group
	has_many :privilege_group_users
	has_many :users,:through=>:privilege_group_users
end
