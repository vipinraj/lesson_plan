class PrivilegeGroupUser < ActiveRecord::Base
	belongs_to :user
	belongs_to :privilege_group
end
