class CreateJointTableSubjectTopics < ActiveRecord::Migration
  def self.up
    create_table :subject_topics , :id => false do |t|
      t.references :subject
      t.references :topic
    end
    add_index :subject_topics, [:subject_id,:topic_id]
  end

  def self.down
  end
end
