class AddFieldsToOnlineExamAttendances < ActiveRecord::Migration
  def self.up
    add_column :online_exam_attendances, :ip_address, :string
    add_column :online_exam_attendances, :session_id, :string
    add_column :online_exam_attendances, :current_page, :string
  end

  def self.down

  end
end
