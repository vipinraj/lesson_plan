class CreatePrivilegeGroups < ActiveRecord::Migration
  def self.up
    create_table :privilege_groups do |t|
    	t.string :name
    	t.text :privilege_name
        t.text :allowed_privilege

      t.timestamps
    end
  end

  def self.down
    drop_table :privilege_groups
  end
end
