class AddFieldToOnlineExamAttendance < ActiveRecord::Migration
  def self.up
    add_column :online_exam_attendances, :extra_time, :decimal,:precision => 8, :scale => 2, :default=> 0
  end

  def self.down
  end
end
