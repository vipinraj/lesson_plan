class AddingLinkToLessonPlanInMainMenu < ActiveRecord::Migration
  def self.up
    academics_category = MenuLinkCategory.find_by_name("academics")
    MenuLink.create(:name=>'lesson_plan',:target_controller=>'topics',:target_action=>'home',:higher_link_id=>nil,:icon_class=>"subjects-icon",:link_type=>'general',:user_type=>nil,:menu_link_category_id=>academics_category.id)

  end

  def self.down
  end
end
