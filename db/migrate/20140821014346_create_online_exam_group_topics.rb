class CreateOnlineExamGroupTopics < ActiveRecord::Migration
  def self.up
    create_table :online_exam_group_topics do |t|
      t.references :online_exam_group
      t.references :topic
      t.timestamps
    end
  end

  def self.down
    drop_table :online_exam_group_topics
  end
end
