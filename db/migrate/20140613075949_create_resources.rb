class CreateResources < ActiveRecord::Migration
  def self.up
    create_table :resources do |t|
      t.string :name
      t.string :attachment_file_name
      t.string :attachment_content_type
      t.integer :attachment_file_size
      t.integer :user_id
      t.integer :school_id
      t.timestamps
    end

    create_table :topic_resources , :id => false do |tr|
      tr.references :topic
      tr.references :resource
    end
    add_index :topic_resources, [:resource_id,:topic_id]

  end

  def self.down
    drop_table :resources
  end
end
