

menu_link_present = MenuLink rescue false
unless menu_link_present == false

  academics_category = MenuLinkCategory.find_by_name("academics")
  MenuLink.create(:name=>'lesson_plan',:target_controller=>'topics',:target_action=>'home',:higher_link_id=>nil,:icon_class=>"subjects-icon",:link_type=>'general',:user_type=>nil,:menu_link_category_id=>academics_category.id)  unless MenuLink.exists?(:name=>'lesson_plan')

end




#Creating_privileges_in_privilege_group
PrivilegeGroup.find_or_create_by_name(:name=>"cio",:privilege_name=>["ExaminationControl", "EnterResults", "ViewResults", "Admission", "StudentsControl", "ManageNews", "ManageTimetable", "StudentAttendanceView", "HrBasics", "ManageCourseBatch", "SubjectMaster", "EventManagement", "GeneralSettings", "FinanceControl", "TimetableView", "StudentAttendanceRegister", "EmployeeAttendance", "PayslipPowers", "EmployeeSearch", "SMSManagement", "StudentView", "ApplicantRegistration", "BlogAdmin", "CustomImport", "CustomReportControl", "CustomReportView", "DataManagement", "DataManagementViewer", "Discipline", "GroupCreate", "SendEmail", "EmailAlertSettings", "Gallery", "HostelAdmin", "InventoryManager", "Inventory", "InventoryBasics", "Librarian", "Tokens", "Oauth2Manage", "PlacementActivities", "PollControl", "TaskManagement", "TransportAdmin", "ReportsView", "FormBuilder", "OnlineExamControl", "ClassroomAllocation", "ManageBuilding", "ManageBuildingAndAllocation","ManageUsers"],:allowed_privilege_group=>["registrar","director_of_finance","hr","dean_academics"])
PrivilegeGroup.find_or_create_by_name(:name=>"registrar",:privilege_name=>["CustomReportsControl","CustomReportsview","ViewResult","AdditionalReportsView","HrBasics","ManageNews","TimetableView","ManageUsers"],:allowed_privilege=>["Librarian","HrBasics","Finance","ExaminationControl","ManageNews",],:allowed_privilege_group=>["assistant_registrar_academics"])
PrivilegeGroup.find_or_create_by_name(:name=>"assistant_registrar_academics",:privilege_name=>["Discipline","StudentAttendanceView","ManageNews","TimetableView","BlogAdmin","Gallery","ManageUsers"],:allowed_privilege_group=>[""])
PrivilegeGroup.find_or_create_by_name(:name=>"director_of_finance",:privilege_name=>["PayslipPowers","FinanceControl","ManageNews","ManageUsers"])
PrivilegeGroup.find_or_create_by_name(:name=>"hr",:privilege_name=>["HrBasics","EmployeeAttendance","PayslipPowers","ManageNews","TimetableView","EmailAlertSettings",],:allowed_privilege_group=>["faculty"])
PrivilegeGroup.find_or_create_by_name(:name=>"dean_academics",:privilege_name=>["ManageCourseBatch","SubjectMaster","CustomImport","CustomReportsview","CustomReportsControl","Discipline","Admission","StudentsControl","StudentView","StudentAttendanceRegister","StudentAttendanceView","ApplicantRegistration","ManageNews","ManageBuilding","ManageBuildingAndAllocation","ManageTimetable","ManageUsers"])
PrivilegeGroup.find_or_create_by_name(:name=>"faculty",:privilege_name=>["GroupCreate","PollControl"])

